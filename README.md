# HDCore - docker-flake8

## Introduction

This is a small container image that contains a python3 environment with flake8 and pytest. This container is very usefull for automatic testing during CI.

## Image usage

- Run pylint:

```bash
docker run --rm -v /path/to/python/code:/code hdcore/docker-flake8:<version> flake8 <arguments>
```

- Run shell:

```bash
docker run -it --rm -v /path/to/python/code:/code hdcore/docker-flake8:<version> /bin/sh
```

- Use in .gitlab-ci.yml:

```bash
image: hdcore/docker-flake8:<version>
script: flake8 <arguments>
```

## Available tags

- hdcore/docker-flake8:7

## Container Registries

The image is stored on multiple container registries at dockerhub and gitlab:

- Docker Hub:
  - hdcore/docker-flake8
  - registry.hub.docker.com/hdcore/docker-flake8
- Gitlab:
  - registry.gitlab.com/hdcore-docker/docker-flake8

## Building image

Build:

```bash
docker build -f <version>/Dockerfile -t docker-flake8:<version> .
```
